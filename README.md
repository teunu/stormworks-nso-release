# Stormworks NSO
<b>North Sawyer Overhaul</b> (NSO) aims to improve the overworld gameplay within Stormworks. This is done by expanding on the barren wastes of the "mega island", adding more infrastructure, locations, edit zones, and industry spots. If you find yourself editing and creating vehicles endlessly, without much fun using them in the open world, then this mod is for you!

<b>NSO Will:</b>
- Update and rework the roads and rails around the main island to add a wider range of gameplay and player decisions.
- Add new locations, containing new edit zones, challenges, and landmarks for you and your creations to visit.
- Optimise Physics and add basic LOD, to smooth your experience playing the game

[![Alpha 2 Video](http://img.youtube.com/vi/K7UQA1VMQu0/0.jpg)](https://www.youtube.com/watch?v=K7UQA1VMQu0 "NSO Alpha 2 Update")

<h3>Alpha 2.0: Roads and Rails test</h3>
In Alpha 2.0 we're focusing on the roads and rails. As this is an alpha, feedback and bug reports will be read and used for the final version of NSO. 
Help us make NSO better and stronger!

> 2.0 and 1.x versions are not compatible. 1.x areas will be revisited and redone with the knowledge we're gaining from the Alpha versions.

<h3>Missions</h3>
Most vanilla missions will not be compatible, we recommend disabling all missions, and using only the `[NSO] Objects` mission.
It exposes the following commands:

`?nso spawn FILTER`
`?nso despawn FILTER`
To (de)spawn things based on a filter (see below).
Note! not providing a filter to either spawn or despawn will spawn or despawn everything.
You don't need to worry about spawning things twice: it prevents duplicates for static vehicles.

<b>Catenary</b>
There are multiple sections of catenary, `nso spawn Cat` can be used to (de)spawn all.
The sections are called `Cat1`, `Cat2`, up to `Cat5`.

<b>Level of Detail</b>
Level of detail things like road markings.
`?nso spawn LOD`

<b>Railway switches and signals</b>
`?nso spawn signalling_equipment`
Note: the current version of the junction does *not* support the radio command on channel 440. This will be fixed in an update.
Note: the railway signals don't work yet.


<b>Traffic lights:</b>
Due to technical reasons this is a different command:
`?nso traffic spawn`
`?nso traffic despawn`
`?nso traffic reset` If the lights misbehave they can be reset with this command.

<h3>How to Install:</h3>
-> Download your preferred NSO release <br>
-> Right click your "Stormworks" ribbon in steam, and click "browse local files" <br>
-> Paste the contents of the NSO release into this folder. If it asks to overwrite, allow it! Those are the new tiles being placed. <br>
-> Open up your game!

<h3>How to uninstall:</h3>
If you want to return to the base game, run NSO_uninstall.exe <br>
Follow the yellow prompts on the NSO uninstaller. <br>
Make sure to verify game files! This is the most important step! <br>
> The uninstaller is open sourced on <a href="https://github.com/teunu/NSO-Uninstaller"> GitHub </a>. This helps keep everyone secure.

<h3>Show your Support and Appreciation!</h3>
We're a standalone team, working on NSO in our free time. We hope that our activity in the modding scene improves the Stormworks community as a whole.
When we see mention and use of NSO, or get feedback and questions about it, we get motivated! So please consider making mention to your friends and showing off your enjoyment on sharing platforms.

In case you want to chip in for the coffee, tea and nutrition we consume while working on this mod and the tools behind it, we've created a Ko-Fi for pay-what-you-want donations! 

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/B0B4QCPOE)